// pages/user.js
import {
  getListData
} from '../../service/homeService'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    otherData: {},
    onUseData: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  createContent() {
    return Math.random().toString().slice(-10)
  },
  getList() {
    getListData().then((res = []) => {
      this.setData({
        list: [...this.data.list, ...res],
      })
    })
  },
  addmore() {
    this.getList()
  }
})