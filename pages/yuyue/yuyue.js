// pages/yuyue/yuyue.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    formData: {
      nickName: '',
      phone: null,
      email: null
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    if (app.globalData.userInfo) {

      this.setData({
        userInfo: app.globalData.userInfo,
        formData: {
          ...this.data.formData,
          nickName: app.globalData.userInfo.nickName
        }
      })
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          console.log(res)
          app.globalData.userInfo = res.userInfo
          this.setData({
            formData: {
              ...this.data.formData,
              nickName: res.userInfo.nickName
            }
          })
        }
      })
    }
  },
  inputValue(e) {
    const key = e.target.dataset.key
    const value = e.detail.value
    this.setData({
      formData: {
        ...this.data.formData,
        [key]: value
      }
    })
  },

  submit() {
    console.log(this.data.formData)
    wx.showToast({
      title: '预约成功',
      success: () => {
        this.setData({
          formData: {
            ...this.data.formData,
            phone: null,
            email: null
          }
        })
      }
    })

    setTimeout(() => {
      wx.hideToast()
    }, 1000)
  }
})