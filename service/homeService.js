let id = 1;
const createContent = () => {
  return Math.random().toString().slice(-10)
}

export const getListData = () => {
  return new Promise((res, rej) => {
    setTimeout(() => {
      const list = new Array(10).fill(1).map(() => ({
        id: id++,
        content: createContent()
      }))
      res(list)
    }, 300)
  })
}